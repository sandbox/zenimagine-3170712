<?php

namespace Drupal\user_invitation\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Session\AccountInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class UserInvitationPageController extends ControllerBase {

  protected $route_match;

  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    $instance->route_match = $container->get('current_route_match');
    return $instance;
  }

  public function Invitation() {
    return [
      '#theme' => 'user_invitation_page_template',
    ];
  }

  public function invitationAccess(AccountInterface $account) {
    $uid = $this->route_match->getParameter('user');
    return AccessResult::allowedIf($account->id() == $uid)
      ->orIf(AccessResult::allowedIfHasPermission($account, 'administer users'));
  }

}